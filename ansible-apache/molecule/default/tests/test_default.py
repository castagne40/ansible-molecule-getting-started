import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_pkg_apache(host):
    pkg = host.package('apache2')

    assert pkg.is_installed


def test_listen_80(host):
    port = host.socket("tcp://80")

    assert port.is_listening


def test_curl_return(host):
    cmd = "curl localhost"
    c = host.run(cmd)

    assert "Hello World" in c.stdout
