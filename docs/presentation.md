# Présentation

## Ansible

Ansible apparait en 2012 et se fera racheter par Red Hat en 2015.

C'est une solution permettant de réaliser des déploiements, l’exécution de tâches et la gestion de configuration sur plusieurs machines en même temps. Il est agent-less et utilise SSH pour mettre en place les actions à réaliser, elles-mêmes écrites en YAML.

Dans Ansible, il existe de nombreux produits qui peuvent perturber lorsqu’on commence. Vous entendrez peut-être parler de Ansible Playbook, Ansible Vault et Ansible Galaxy.

Gérer un parc de machines avec les commandes c’est déjà bien, et c’est surtout la base d’Ansible. Malgré ça, on imagine mal jouer un scénario en exécutant des commandes les unes à la suite des autres. Heureusement pour nous, ansible-playbook est là pour ça !

Dans certains cas, vous aurez besoin de stocker dans vos scénarios des informations sensibles (mot de passe…). Plutôt que de stocker en clair ces informations, vous pouvez encoder/décoder ses fichiers grâce à Ansible Vault.

Ansible-Galaxy est un Hub pour partager vos modules. Il fonctionne un peu à la manière de Docker Hub pour les images Docker. Chez cdiscount, on préfère utiliser des git submodules.

## TDD (Test Driven Development) et Molecule

Molecule permet le développement d'un rôle Ansible piloté par les tests grâce à de nombreux outils comme docker ou vragrant et testinfra (script python qui permet de faire de vérifications sur le serveur)

Qu'est-ce que le TDD ? Cette méthode consiste à rédiger les tests unitaires avant de procéder à la phase de codage.

Quels sont les avantages du TDD ? Les tests unitaires sont réellement écrits. Le développeur a une sensation d'accomplissement, il s'agit d'une sorte de défis de faire valider les tests. Les détails de l'interface et le comportement sont clarifiés.

Quels sont les avantages des tests unitaires ? Le fait de disposer d'un grand nombre de tests permett de s'assurer de la solidité et garantie du code. Non présence de régression du code.

Les tests de Molecule peuvent être intégrés à un système de CI/CD.
