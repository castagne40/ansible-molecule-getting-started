# ANSIBLE/MOLECULE Getting Started

## Prerequisite

```
sudo apt-get install virtualenv python-pip
virtualenv venv
source venv/bin/activate
pip install molecule==2.19.0 ansible==2.7.2 docker-py
```

## Init Molecule

```
molecule init role -r ansible-apache -d docker
cd ansible-apache
molecule test
```

## Insight

### Video

"kazam" is used for terminal record (mp4 format)

### PS1

Simplified PS1 :

```
export PS1="\
\[$bldgrn\]\s>\[$bldblu\]/\W/\[$bldylw\]\\[$txtrst\]$ \
"
```
